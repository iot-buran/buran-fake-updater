const EVENT_SOCKET_URL = process.env.EVENT_SOCKET_URL || 'http://localhost:3333'
const UPDATER_PORT = 1818;

const DEVICE_TYPE = "FakeDevice"
let net = require('net');

let faye = require('faye');
let eventSocket = new faye.Client(EVENT_SOCKET_URL);

let createServer = () => {
    let connectedDeviceRegistry = {};

    let pendingDevicesForUpdate = {}

    let server = net.createServer((socket) => {
        socket.setNoDelay(true)
        socket.on('connect', () => console.log("A device connected to the server"))
        socket.on('error', err => console.log(`Error while talking to device ${err}`))
        socket.on('close', hadError => {
            for (devId in connectedDeviceRegistry) {
                if (connectedDeviceRegistry[devId].openSocket === socket) {
                    delete connectedDeviceRegistry[devId]
                }
            }
            if (hadError) {
                console.log("Socket closed due to error.")
            }
        })
        socket.on('timeout', () => {
            console.log(`Socket timed out`)
            socket.destroy()
        })

        socket.on('data', data => {
            let dataType = data.toString().split(" ")[0];
            if (dataHandlers[dataType]) {
                dataHandlers[dataType](data.toString(), socket);
            } else {
                console.log(`Uknown message type: ${dataType}`)
            }
            let possibleDeviceId =  data.toString().split(" ")[1];
            if (connectedDeviceRegistry[possibleDeviceId]) {
                onHeartBeat(data.toString().split(" ")[1])
            }
        })
    });

    server.on('error', err => {
        console.log(`Server error ${err}`)
    })
    server.listen(UPDATER_PORT, () => {
        console.log(`Server listening on port ${UPDATER_PORT}`)
    })
    let connectDevice = (data, socket) => {
        let splitData = data.split(" ")
        let deviceId = splitData[1];
        let newDevice = {firmwareVersion: splitData[2], openSocket: socket};
        connectedDeviceRegistry[deviceId] = newDevice
    
        socket.write("CONNECTED\n")
    
        console.log(`New device connected: ${deviceId}:${newDevice.firmwareVersion}`)
        emitDeviceConnectEvent(deviceId, splitData[2], DEVICE_TYPE, splitData[3])

        if(pendingDevicesForUpdate[deviceId]) {
            let pendingDevice = pendingDevicesForUpdate[deviceId]
            scheduleUpdate(deviceId, pendingDevice.newVersion, pendingDevice.campaignId)
            delete pendingDevicesForUpdate[deviceId]
        }
    }
 
    let disconnectDevice = (data, socket) => {
        let splitData = data.split(" ");
        let deviceId = splitData[1];        console.log(`Destroying socket for device ${deviceId} and removing device due to inactivity`)
        if (connectedDeviceRegistry[deviceId]['openSocket']) {
            connectedDeviceRegistry[deviceId]['openSocket'].destroy();
        }
        delete connectedDeviceRegistry[deviceId]
        emitDeviceDisconectEvent(deviceId)
    }    

    let updateSuccessful = (data, socket) => {
        let splitData = data.split(" ");
        let deviceId = splitData[1];
        connectedDeviceRegistry[deviceId]['updating'] = false
        console.log(`Update for device ${deviceId} was successful`)
        emitDeviceUpdateSuccessfulEvent(deviceId, splitData[2])
    }
    
    
    let updateFailed = (data, socket) => {
        let splitData = data.split(" ");
        let deviceId = splitData[1];
        connectedDeviceRegistry[deviceId]['updating'] = false;

        console.log(`Update for device ${deviceId} failed`)
        emitDeviceUpdateFailedEvent(deviceId, splitData[2])
    }

    let heartBeatReceived = (data, socket) => {
        console.log(`Received heartbeat from ${data.split(" ")[1]}`)
    }
    let regularMessageReceived = (data, socket) => {
        console.log(`Received message from ${data.split(" ")[1]}: ${data.split(" ")[2]}`)
    }
    
    let onHeartBeat = (deviceId) => {
        connectedDeviceRegistry[deviceId]['lastHeartBeat'] = new Date().getTime();     
    }

    let scheduleUpdate = async (deviceId, firmwareVersion, campaignId) => {
        let device = connectedDeviceRegistry[deviceId];
        if (!device['updating']) {
                
            device.openSocket.write(`UPDATE ${firmwareVersion} ${campaignId}`)
            device['updating'] = true;
            emitDeviceUpdatingEvent(deviceId)

        } else {
            console.log(`Device ${deviceId} is already being updated`)
        }

    }
    
    
    let dataHandlers = {
        "CONNECT": connectDevice,
        "DISCONNTECT": disconnectDevice,
        "UPDATESUCCESSFUL": updateSuccessful,
        "TUPTUP": heartBeatReceived,
        "MESSAGE": regularMessageReceived,
        "UPDATEFAILED": updateFailed
    }
    
    eventSocket.subscribe(`/update-device-${DEVICE_TYPE}`, data => {
        if (connectedDeviceRegistry[data.deviceId]) {
            if (connectedDeviceRegistry[data.deviceId].firmwareVersion == data.newVersion) {
                eventSocket.publish('/device-updatenotneeded', {deviceId: data.deviceId, campaignId: data.campaignId})
            } else {
                scheduleUpdate(data.deviceId, data.newVersion, data.campaignId)
            }
        } else {
            pendingDevicesForUpdate[data.deviceId] = {campaignId: data.campaignId, firmwareUrl:data.firmwareUrl, newVersion:data.newVersion}
        }
    })

    eventSocket.subscribe(`/unschedule-device-${DEVICE_TYPE}`, data => {
        if (pendingDevicesForUpdate[data.deviceId]) {
            delete pendingDevicesForUpdate[data.deviceId]
        }
    })    
}

let emitDeviceConnectEvent = (deviceId, firmwareVersion, type, userId) => {
    eventSocket.publish('/device-connect', {deviceId: deviceId, user: userId, firmwareVersion: firmwareVersion, type: type})
}

let emitDeviceDisconectEvent = deviceId => {
    eventSocket.publish('/device-disconnect', {deviceId: deviceId})
}

let emitDeviceUpdatingEvent = deviceId => {
    eventSocket.publish('/device-updating', {deviceId: deviceId})
}

let emitDeviceUpdateSuccessfulEvent = (deviceId, campaignId) => {
    eventSocket.publish('/device-updatesuccess', {deviceId: deviceId, campaignId: campaignId})
}

let emitDeviceUpdateFailedEvent = (deviceId, campaignId) => {
    eventSocket.publish('/device-updatedfailed', {deviceId: deviceId, campaignId: campaignId})
}

createServer();

