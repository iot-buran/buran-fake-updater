# buran-fake-updater

Buran updater module for Fake devices

# Docker Run

1. Build

docker build -t buran-fake-updater .

2. Run

docker run -d -it -p {SPECIFY_SOCKET_PORT}:8080 -e EVENT_SOCKET_URL={SPECIFY_SOCKET_UEL} buran-fake-updater

Example:

docker run -d -it -p 2020:8080 -e EVENT_SOCKET_URL=http://192.168.104.199:1111 buran-fake-updater